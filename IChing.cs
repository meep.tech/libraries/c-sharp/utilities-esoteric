﻿using System;

namespace Meep.Tech.Utilities.Esoteric {

  /// <summary>
  /// Constants relating to IChing
  /// </summary>
  public static class IChing {

    /// <summary>
    /// The duality
    /// </summary>
    public enum Duality {
      Yin, // broken
      Yang // unbroken
    }

    /// <summary>
    /// The 8 trigrams
    /// </summary>
    public enum Trigrams {
      CreativeHeavens,
      JoyousLake,
      ClingingFire,
      ArousingThunder,
      GentleWind,
      AbysmalWater,
      StillMountain,
      ReceptiveEarth
    }

    /// <summary>
    /// The 64 hexagrams of IChing
    /// Indexed at 0 in traditional order
    /// </summary>
    public enum Hexagrams {
      Creative,
      Receptive,
      DifficultyInTheBeginning,
      YouthfulFolly,
      Waiting,
      Conflict,
      Discipline,
      HoldingTogether,
      TamingPowerOfTheSmall,
      Treading,
      Peace,
      Standstill,
      Companionship,
      Prosperity,
      Modesty,
      Enthusiasm,
      Following,
      WorkOnWhatHasBeenSpoiled,
      Approach,
      Contemplation,
      BitingThrough,
      Grace,
      SplittingApart,
      TurningPoint,
      Innocence,
      TamingPowerOfTheGreat,
      CornersOfTheMouth,
      Excess,
      Abysmal,
      Clinging,
      Influence,
      Duration,
      Retreat,
      GreatPower,
      Progress,
      DarkeningOfTheLight,
      Family,
      Opposition,
      Obstacles,
      Deliverance,
      Decrease,
      Increase,
      Resoluteness,
      AttractionOfOpposites,
      GatheringTogether,
      PushingUpward,
      Oppression,
      TheWell,
      Revolution,
      Cauldron,
      Arousing,
      KeepingStill,
      Development,
      MarryingMaiden,
      Abundance,
      Wanderer,
      Gentle,
      Joyous,
      Dispersion,
      Limitation,
      InnerTruth,
      SmallIsBeautiful,
      Completion,
      BeforeCompletion
    }

    /// <summary>
    /// Lines in the trigrams
    /// From bottom to top.
    /// </summary>
    internal static Duality[][] DualitiesPerTrigram => new Duality[][] {
      new Duality[] {
        Duality.Yang,
        Duality.Yang,
        Duality.Yang
      },
      new Duality[] {
        Duality.Yang,
        Duality.Yang,
        Duality.Yin
      },
      new Duality[] {
        Duality.Yang,
        Duality.Yin,
        Duality.Yang,
      },
      new Duality[] {
        Duality.Yang,
        Duality.Yin,
        Duality.Yin
      },
      new Duality[] {
        Duality.Yin,
        Duality.Yang,
        Duality.Yang
      },
      new Duality[] {
        Duality.Yin,
        Duality.Yang,
        Duality.Yin,
      },
      new Duality[] {
        Duality.Yin,
        Duality.Yin,
        Duality.Yang
      },
      new Duality[] {
        Duality.Yin,
        Duality.Yin,
        Duality.Yin
      }
    };

    /// <summary>
    /// Trigrams in each Hexagram
    /// 0 => first trigram, the base/bottom/below
    /// 1 => second trigram, the top/upper/above
    /// </summary>
    internal static Trigrams[][] TrigramsPerHexagram => new Trigrams[][] {
      new Trigrams[]{
        Trigrams.CreativeHeavens,
        Trigrams.CreativeHeavens
      },
      new Trigrams[]{
        Trigrams.ReceptiveEarth,
        Trigrams.ReceptiveEarth
      },
      new Trigrams[]{
        Trigrams.ArousingThunder,
        Trigrams.AbysmalWater
      },
      new Trigrams[]{
        Trigrams.AbysmalWater,
        Trigrams.StillMountain
      },
      new Trigrams[]{
        Trigrams.CreativeHeavens,
        Trigrams.AbysmalWater
      },
      new Trigrams[]{
        Trigrams.AbysmalWater,
        Trigrams.CreativeHeavens
      },
      new Trigrams[]{
        Trigrams.AbysmalWater,
        Trigrams.ReceptiveEarth
      },
      new Trigrams[]{
        Trigrams.ReceptiveEarth,
        Trigrams.AbysmalWater
      },
      new Trigrams[]{
        Trigrams.CreativeHeavens,
        Trigrams.GentleWind
      },
      new Trigrams[]{ // 10
        Trigrams.JoyousLake,
        Trigrams.CreativeHeavens
      },
      new Trigrams[]{
        Trigrams.CreativeHeavens,
        Trigrams.ReceptiveEarth
      },
      new Trigrams[]{
        Trigrams.ReceptiveEarth,
        Trigrams.CreativeHeavens
      },
      new Trigrams[]{
        Trigrams.ClingingFire,
        Trigrams.CreativeHeavens
      },
      new Trigrams[]{
        Trigrams.CreativeHeavens,
        Trigrams.ClingingFire
      },
      new Trigrams[]{
        Trigrams.StillMountain,
        Trigrams.ReceptiveEarth
      },
      new Trigrams[]{
        Trigrams.ReceptiveEarth,
        Trigrams.ArousingThunder
      },
      new Trigrams[]{
        Trigrams.ArousingThunder,
        Trigrams.JoyousLake
      },
      new Trigrams[]{
        Trigrams.GentleWind,
        Trigrams.StillMountain
      },
      new Trigrams[]{
        Trigrams.JoyousLake,
        Trigrams.ReceptiveEarth
      },
      new Trigrams[]{ // 20
        Trigrams.ReceptiveEarth,
        Trigrams.GentleWind
      },
      new Trigrams[]{
        Trigrams.ArousingThunder,
        Trigrams.ClingingFire
      },
      new Trigrams[]{
        Trigrams.ClingingFire,
        Trigrams.StillMountain
      },
      new Trigrams[]{
        Trigrams.ReceptiveEarth,
        Trigrams.StillMountain
      },
      new Trigrams[]{
        Trigrams.ArousingThunder,
        Trigrams.ReceptiveEarth
      },
      new Trigrams[]{
        Trigrams.ArousingThunder,
        Trigrams.CreativeHeavens
      },
      new Trigrams[]{
        Trigrams.CreativeHeavens,
        Trigrams.StillMountain
      },
      new Trigrams[]{
        Trigrams.ArousingThunder,
        Trigrams.StillMountain
      },
      new Trigrams[]{
        Trigrams.GentleWind,
        Trigrams.JoyousLake
      },
      new Trigrams[]{
        Trigrams.AbysmalWater,
        Trigrams.AbysmalWater
      },
      new Trigrams[]{ // 30
        Trigrams.ClingingFire,
        Trigrams.ClingingFire
      },
      new Trigrams[]{
        Trigrams.StillMountain,
        Trigrams.JoyousLake
      },
      new Trigrams[]{
        Trigrams.GentleWind,
        Trigrams.ArousingThunder
      },
      new Trigrams[]{
        Trigrams.StillMountain,
        Trigrams.CreativeHeavens
      },
      new Trigrams[]{
        Trigrams.CreativeHeavens,
        Trigrams.ArousingThunder
      },
      new Trigrams[]{
        Trigrams.ReceptiveEarth,
        Trigrams.ClingingFire
      },
      new Trigrams[]{
        Trigrams.ClingingFire,
        Trigrams.ReceptiveEarth
      },
      new Trigrams[]{
        Trigrams.ClingingFire,
        Trigrams.GentleWind
      },
      new Trigrams[]{
        Trigrams.JoyousLake,
        Trigrams.ClingingFire
      },
      new Trigrams[]{
        Trigrams.StillMountain,
        Trigrams.AbysmalWater
      },
      new Trigrams[]{ // 40
        Trigrams.AbysmalWater,
        Trigrams.ArousingThunder
      },
      new Trigrams[]{
        Trigrams.JoyousLake,
        Trigrams.StillMountain
      },
      new Trigrams[]{
        Trigrams.ArousingThunder,
        Trigrams.GentleWind
      },
      new Trigrams[]{
        Trigrams.CreativeHeavens,
        Trigrams.JoyousLake
      },
      new Trigrams[]{
        Trigrams.GentleWind,
        Trigrams.CreativeHeavens
      },
      new Trigrams[]{
        Trigrams.ReceptiveEarth,
        Trigrams.JoyousLake
      },
      new Trigrams[]{
        Trigrams.GentleWind,
        Trigrams.ReceptiveEarth
      },
      new Trigrams[]{
        Trigrams.AbysmalWater,
        Trigrams.JoyousLake
      },
      new Trigrams[]{
        Trigrams.GentleWind,
        Trigrams.AbysmalWater
      },
      new Trigrams[]{
        Trigrams.ClingingFire,
        Trigrams.JoyousLake
      },
      new Trigrams[]{ // 50
        Trigrams.GentleWind,
        Trigrams.ClingingFire
      },
      new Trigrams[]{
        Trigrams.ArousingThunder,
        Trigrams.ArousingThunder
      },
      new Trigrams[]{
        Trigrams.StillMountain,
        Trigrams.StillMountain
      },
      new Trigrams[]{
        Trigrams.StillMountain,
        Trigrams.GentleWind
      },
      new Trigrams[]{
        Trigrams.JoyousLake,
        Trigrams.ArousingThunder
      },
      new Trigrams[]{
        Trigrams.ClingingFire,
        Trigrams.ArousingThunder
      },
      new Trigrams[]{
        Trigrams.StillMountain,
        Trigrams.ClingingFire
      },
      new Trigrams[]{
        Trigrams.GentleWind,
        Trigrams.GentleWind
      },
      new Trigrams[]{
        Trigrams.JoyousLake,
        Trigrams.JoyousLake
      },
      new Trigrams[]{
        Trigrams.AbysmalWater,
        Trigrams.GentleWind
      },
      new Trigrams[]{ // 60
        Trigrams.JoyousLake,
        Trigrams.AbysmalWater
      },
      new Trigrams[]{
        Trigrams.JoyousLake,
        Trigrams.GentleWind
      },
      new Trigrams[]{
        Trigrams.StillMountain,
        Trigrams.ArousingThunder
      },
      new Trigrams[]{
        Trigrams.ClingingFire,
        Trigrams.AbysmalWater
      },
      new Trigrams[]{
        Trigrams.AbysmalWater,
        Trigrams.ClingingFire
      }
    };
  }

  /// <summary>
  /// Extensions for hexagrams
  /// </summary>
  public static class HexagramExtensions {

    /// <summary>
    /// Get a hexagrams traditional # (King Wei)
    /// </summary>
    public static int GetTraditionalNumber(this IChing.Hexagrams hexagram) {
      return (int)hexagram + 1;
    }

    /// <summary>
    /// Get a hexagrams Trigrams, bottom then top
    /// </summary>
    public static (IChing.Trigrams below, IChing.Trigrams above) GetTrigrams(this IChing.Hexagrams hexagram) {
      var (below, above, _) = IChing.TrigramsPerHexagram[(int)hexagram];
      return (below, above);
    }

    /// <summary>
    /// Get the hexagram made of the given trigrams
    /// </summary>
    /// <param name="trigrams"></param>
    /// <returns></returns>
    public static IChing.Hexagrams GetHexagram(this (IChing.Trigrams below, IChing.Trigrams above) trigrams) {
      for (int hexagramIndex = 0; hexagramIndex < 64; hexagramIndex++) {
        var (below, above, _) = IChing.TrigramsPerHexagram[hexagramIndex];
        if (trigrams.above == above && trigrams.below == below) {
          return (IChing.Hexagrams)hexagramIndex;
        }
      }
      
      throw new Exception($"Could not find a hexagram made of below: {trigrams.below}, and above: {trigrams.above}, trigrams.");
    }

    /// <summary>
    /// Get the hexagram made of the given trigrams
    /// </summary>
    /// <param name="trigrams"></param>
    /// <returns></returns>
    public static IChing.Trigrams GetTrigram(this (IChing.Duality bottom, IChing.Duality middle, IChing.Duality top) dualityLines) {
      for (int trigramIndex = 0; trigramIndex < 64; trigramIndex++) {
        var (bottom, middle, (top,  _)) = IChing.DualitiesPerTrigram[trigramIndex];
        if (dualityLines.bottom == bottom && dualityLines.middle == middle && dualityLines.top == top) {
          return (IChing.Trigrams)trigramIndex;
        }
      }
      
      throw new Exception($"Could not find a trigram made of bottom: {dualityLines.bottom}, middle: {dualityLines.middle}, and top: {dualityLines.top}, duality lines.");
    }

    /// <summary>
    /// Get a Trigram's Duality lines
    /// </summary>
    public static (IChing.Duality bottom, IChing.Duality middle, IChing.Duality top) GetDualities(this IChing.Trigrams trigram) {
      var (bottom, (middle, (top, _))) = IChing.DualitiesPerTrigram[(int)trigram];
      return (bottom, middle, top);
    }

    /// <summary>
    /// Convert a base64 char to it's hexagram
    /// </summary>
    /// <param name="base64Character"></param>
    /// <returns></returns>
    public static IChing.Hexagrams AsHexagram(this char base64Character) {
     return (IChing.Hexagrams)base64Character.ToString().ToIntFromBase64();
    }

    /// <summary>
    /// Convert a hexagram to it's base64 char
    /// </summary>
    /// <param name="base64Character"></param>
    /// <returns></returns>
    public static char AsChar(this IChing.Hexagrams hexagram) {
     return Convert.ToBase64String(BitConverter.GetBytes((int)hexagram))[0];
    }

    /// <summary>
    /// Get a hexagrams's 6 Duality lines from the bottom up
    /// </summary>
    public static IChing.Duality[] GetDualities(this IChing.Hexagrams hexagram) {
      var (selfBelow, selfAbove) = hexagram.GetTrigrams();
      var (line1, line2, line3) = selfBelow.GetDualities();
      var (line4, line5, line6) = selfAbove.GetDualities();

      return new IChing.Duality[] {
        line1,
        line2,
        line3,
        line4,
        line5,
        line6
      };
    }

    /// <summary>
    /// Get a hexagram from a set of 6 duality lines
    /// </summary>
    public static IChing.Hexagrams MakeHexagram(this IChing.Duality[] dualityLines) {
      if (dualityLines.Length != 6) {
        throw new Exception($"Cannot make a hexagram with anything but 6 duality lines. You tried with {dualityLines.Length}.");
      }

      IChing.Trigrams belowTrigram = (dualityLines[0], dualityLines[1], dualityLines[2]).GetTrigram();
      IChing.Trigrams aboveTrigram = (dualityLines[3], dualityLines[4], dualityLines[5]).GetTrigram();
      return (belowTrigram, aboveTrigram).GetHexagram();
    }

    /// <summary>
    /// Xor a duality
    /// </summary>
    public static IChing.Duality XOR(this IChing.Duality self, IChing.Duality other) {
      return self == other ? IChing.Duality.Yin : IChing.Duality.Yang;
    }

    /// <summary>
    /// XOR a hexagram
    /// </summary>
    /// <param name="self"></param>
    /// <param name="other"></param>
    /// <returns></returns>
    public static IChing.Hexagrams XOR(this IChing.Hexagrams self, IChing.Hexagrams other) {
      IChing.Duality[] newHexagramLines = new IChing.Duality[6];

      int lineIndex = 0;
      IChing.Duality[] otherLines = other.GetDualities();
      foreach (IChing.Duality duality in self.GetDualities()) {
        newHexagramLines[lineIndex] = duality.XOR(otherLines[lineIndex++]);
      }

      return newHexagramLines.MakeHexagram();
    }
  }
}
