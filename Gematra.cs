﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Tech.Utilities.Esoteric {
  public static class Gematra {

    /// <summary>
    /// Gematria values for english letters
    /// </summary>
    static Dictionary<char, int> GematraMap
      => new Dictionary<char, int>() {
        {'a', 1 },
        {'b', 2 },
        {'c', 3 },
        {'d', 4 },
        {'e', 5 },
        {'f', 6 },
        {'g', 7 },
        {'h', 8 },
        {'i', 9 },
        {'j', 10 },
        {'k', 20 },
        {'l', 30 },
        {'m', 40 },
        {'n', 50 },
        {'o', 60 },
        {'p', 70 },
        {'q', 80 },
        {'r', 90 },
        {'s', 100 },
        {'t', 200 },
        {'u', 300 },
        {'v', 400 },
        {'w', 500 },
        {'x', 600 },
        {'y', 700 },
        {'z', 800 }
      };

    /// <summary>
    /// Get the total value of a word from it's letters added up
    /// </summary>
    public static int WordValue(string word) {
      int total = 0;
      foreach (char letter in word.ToLower()) {
        // if it's just an int, just add it
        if (int.TryParse(letter.ToString(), out int number)) {
          total += number;
        // if it's in the gematra map, add it
        } else if (GematraMap.TryGetValue(letter, out int convertedValue)) {
          total += convertedValue;
        }
      }

      return total;
    }

  }
}
